package wordTree.driver;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Results implements FileDisplayInterface, StdoutDisplayInterface {
    private String file;
    
    /** Empty Constructor for Results
    * @param (void)
    * @return (void)
    */
    public Results() {
        Driver.logger.writeMessage("Instance of Results created.", MyLogger.DebugLevel.CONSTRUCTOR);
    }
    
    /** Mutator for fileName
    * @param (String) fileName - The name of the output file specified from the command line
    * @return (void)
    */
    public void setFile(String fileName) {
        file = fileName;
    }
    
    /** Writes a string an output file
    * Creates a BufferedWriter based on the fileName
    * If there is no issue with the file, write the string to the file and close the file
    * Otherwise print an error message and quit
    * @param (String) output - The string to be written to the output file
    * @return (void)
    */
    public void writeSchedulesToFile(String output) {
        try{
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(output);
            bw.close();
        }
        catch(FileNotFoundException e) {
            System.err.println("ERROR: Unable to open output file");
            System.exit(0);
        }
        //Catch IO Exception
        catch(IOException e) {
            System.err.println("ERROR: IO Exception");
            System.exit(0);
        }
    }
    
    /** Outputs the results to the screen via MyLogger
    * Takes in the results and passes it to MyLogger using the appropriate DebugLevel
    * @param (String) output - The message to be written to the screen
    * @return (void)
    */
    public void writeToScreen(String output) {
        Driver.logger.writeMessage(output, MyLogger.DebugLevel.RESULT);
    }
}