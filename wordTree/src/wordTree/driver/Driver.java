package wordTree.driver;
import java.util.ArrayList;

public class Driver {
    
    //Instance of logger used for debugging
    public static MyLogger logger = new MyLogger();
    

    /** Main function for the program
    * Validates Command Line arguments
    * Create instances of TreeBuilder, Results, MyLogger, FileProcessor, and CreateWorkers
    * Call PopulateWorkers to read the file and populate the tree
    * Call DeleteWorkers to remove a word from the tree
    * @param (String[]) args - The command line arguments of the program
    * @return (void)
    */
    public static void main(String[] args) {
 
        //Parameters to be filled by command line arguments
        int numDeletes = 0;
        int debugVal = -1;
        
        //Check if number of arguments is valid
        if(args.length != 5) {
            System.err.println("Usage: <Input File> <Output File> <Number of Threads> <Delete Word 1> <Delete Word 2> <Delete Word 3> <Debug Value>");
            System.exit(1);
        }
        
        //Check if all arguments are full
        if(args[0].equals("${arg0}") || args[1].equals("${arg1}") || args[2].equals("${arg2}") || args[3].equals("${arg3}") || args[4].equals("${arg4}")) {
            System.err.println("Usage: <Input File> <Output File> <Number of Threads> <Delete Words> <Logger Level>");
            System.exit(1);      
        }
        
        //Check if argument 3 is an ints
        try{
            numDeletes = Integer.parseInt(args[2]);
        }
        catch(NumberFormatException e) {
            System.err.println("Argument 3 must be an int");
            System.exit(1);
        }  
        
        //Check if argument 5 is an int
        try {
            debugVal = Integer.parseInt(args[4]); 
        }
        catch(NumberFormatException e) {
            System.err.println("Argument 5 must be an int");
            System.exit(1);
        }
        
        //Check that the Number of threads is in the proper range
        if(numDeletes > 3 || numDeletes < 1) {
            System.err.println("Number of Threads must range from 1 to 3");
            System.exit(1);
        }
        
        //Check that the debug value is in the proper range
        if(debugVal > 4 || debugVal < 0) {
            System.err.println("Debug Value must range from 0 to 4");
            System.exit(1);
        }

        //Parse Delete words, make sure there is the correct ammount
        String[] deleteWords = args[3].split(" ");
        if(deleteWords.length != numDeletes) {
            System.err.println("There must be " + numDeletes + " words to delete");
            System.exit(1);
        }
        
        //END OF COMMAND LINE VALIDATION
        
        //Create Logger Instance
        logger.setDebugValue(debugVal);
        
        //Create results instance
        Results results = new Results();
        
        //Create the tree
        TreeBuilder tree = new TreeBuilder();
        
        //Create instance of FileProcessor using the input file
        FileProcessor fp = new FileProcessor();
        
        //Create workers instance
        CreateWorkers workers = new CreateWorkers(numDeletes, tree, fp);
        
        //Create threads for populating threads
        workers.startPopulateWorkers(args[0]);
        
        //Create threads for deleting words
        workers.startDeleteWorkers(deleteWords);

        //Traverse the tree, count instances of words, characters, and unique words
        tree.traverse(tree.getRoot());
        
        //Create output
        String output = tree.toString();
        
        //Write the results to the output file
        results.setFile(args[1]);
        results.writeSchedulesToFile(output);
        results.writeToScreen(output);
        
    }
}