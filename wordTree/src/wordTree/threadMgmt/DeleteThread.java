package wordTree.driver;

public class DeleteThread implements Runnable {
    private int threadNum = -1;
    private String word = "";
    private TreeBuilder tree;
    
    /** Constructor for DeleteThread
    * @param (String) w - The word to be removed from the tree
    * @param (TreeBuilder) - The tree to be worked on
    * @param (int) i - An int used to identify the thread
    * @return (void)
    */
    public DeleteThread(String w, TreeBuilder t, int i) {
        Driver.logger.writeMessage("Instance of DeleteThread created.", MyLogger.DebugLevel.CONSTRUCTOR);
        word = w;
        tree = t;
        threadNum = i;
    }
    
    /** Removes a word from the tree
    * Call the function on tree to remove a word from the tree
    * @param (void)
    * @return (void)
    */
    public synchronized void run() {
        Driver.logger.writeMessage("Run in Delete Thread " + threadNum + " started.", MyLogger.DebugLevel.RUN);
        tree.removeWord(word);
    }   
}