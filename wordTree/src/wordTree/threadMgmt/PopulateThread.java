package wordTree.driver;

import java.io.BufferedReader;
import java.io.IOException;

public class PopulateThread implements Runnable {
    
    private int threadNum = -1;
    private BufferedReader read = null;
    
    private TreeBuilder tree = null;
    private FileProcessor fileProcessor = null;
    
    /** Constructor for PopulateThread
    * @param (BufferedReader) reader - A reader used for reading the file
    * @param (TreeBuilder) t - The tree to be worked on
    * @param (FileProcessor) fp - The fileProcessor to be used for reading the file
    * @param (int) i - A number used to represent the thread
    * @return (void)
    */
    public PopulateThread(BufferedReader reader, TreeBuilder t, FileProcessor fp, int i) {
        Driver.logger.writeMessage("Instance of PopulateThread created.", MyLogger.DebugLevel.CONSTRUCTOR);
        tree = t;
        fileProcessor = fp;
        read = reader;
        threadNum = i;
    }
    
    /** Calls fileProcessor and adds words to the tree
    * Enter an infinite while loop
    * Read a line from the file using fileProcessor
    * If the line is null, break
    * Otherwise, parse the line into individual words
    * Add each word to the tree
    * @param (void)
    * @return (void)
    */
    public synchronized void run() {
        Driver.logger.writeMessage("Run in Populate Thread " + threadNum + " started.", MyLogger.DebugLevel.RUN);
        String line = "";
        while(true) {
            
            line = fileProcessor.readLine(read);
            
            if(line == null) {break;}
            
            String[] wordsInLine = line.split(" ");
            for(int i = 0; i < wordsInLine.length; i++) {
                if(wordsInLine[i].trim().length() > 0) {
                    tree.insertNode(wordsInLine[i]);
                    Driver.logger.writeMessage("Word Processed: " + wordsInLine[i], MyLogger.DebugLevel.TREE_EDIT);
                }
            }
        }
    }
}