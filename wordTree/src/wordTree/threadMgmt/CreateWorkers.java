package wordTree.driver;

import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileReader;
import java.io.File;

public class CreateWorkers {
    
    private int numThreads;
    private TreeBuilder tree = null;
    private FileProcessor fileProcessor = null;
    
    private BufferedReader read = null;
    
    private ArrayList<Thread> popThreadList = new ArrayList<Thread>();
    private ArrayList<Thread> delThreadList = new ArrayList<Thread>();
    
    /** Value Constructor for CreateWorkers
    * @param (int) threads - The number of threads to be created
    * @param (TreeBuilder) t - The tree to be worked on
    * @param (FileProcessor) fp - The fileProcessor to be used for reading a file
    * @return (Node) n - The Node to be added
    */
    public CreateWorkers(int threads, TreeBuilder t, FileProcessor fp) {
        Driver.logger.writeMessage("Instance of CreateWorkers created.", MyLogger.DebugLevel.CONSTRUCTOR);
        numThreads = threads;
        tree = t;
        fileProcessor = fp;
    }
    
    /** Creates and monitors threads for populating the tree
    * Creates a BufferedReader for the file to be read
    * Use a for loop to create the number of desired threads
    * Run the threads
    * Monitor the threads using an ArrayList and wait for them all to finish
    * @param (String) fileName - The name of the file to be read from
    * @return (void)
    */
    public void startPopulateWorkers(String fileName) {
        try {
            read = new BufferedReader(new FileReader(fileName));
        }
        //Catch File Not Found
        catch(FileNotFoundException e) {
            System.err.println("ERROR: Unable to open input file");
            System.exit(0);
        }
        finally {
        }
        
        for(int i = 0; i < numThreads; i++) {
            Runnable thread = new PopulateThread(read, tree, fileProcessor, i);
            Thread worker = new Thread(thread);
            popThreadList.add(worker);
            worker.start();
        }
        for(int i = 0; i < popThreadList.size(); i++) {
            try {
                popThreadList.get(i).join();
            }
            catch(InterruptedException e) {
                System.err.println("Thread Interrupted");
                System.exit(0);
            }
            
        }  
    }
    
    /** Creates and monitors threads for removing words from the tree
    * Use a for loop to create the number of desired threads
    * Run the threads
    * Monitor the threads using an ArrayList and wait for them all to finish
    * @param (String[]) deleteWords - An array of the words to be deleted
    * @return (void)
    */
    public void startDeleteWorkers(String[] deleteWords) {
        for(int i = 0; i < numThreads; i++) {
            Runnable thread = new DeleteThread(deleteWords[i], tree, i);
            Thread worker = new Thread(thread);
            delThreadList.add(worker);
            worker.start();
        }
        for(int i = 0; i < delThreadList.size(); i++) {
            try {
                delThreadList.get(i).join();
            }
            catch(InterruptedException e) {
                System.err.println("Thread Interrupted");
                System.exit(0);
            }    
        }
    }
}