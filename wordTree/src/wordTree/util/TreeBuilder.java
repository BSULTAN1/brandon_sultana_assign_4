package wordTree.driver;

public class TreeBuilder {
    private Node root;
    int numWords;
    int numChars;
    int numUnique;
    
    /** Constructor for TreeBuilder
    * @param (void)
    * @return (void)
    */
    public TreeBuilder() {
        Driver.logger.writeMessage("Instance of TreeBuilder created.", MyLogger.DebugLevel.CONSTRUCTOR);
        root = null;
        numWords = 0;
        numChars = 0;
        numUnique = 0;
    }
    
    /** Accessor for Root
    * @param (void)
    * @return (Node) root - The root Node of the Tree
    */
    public Node getRoot() {
        return root;
    }
    
    /** Inserts a word into the tree
    * Starts at the root
    * Compares the word in the current Node to the word passed in
    * If they are the same, the count of that Node is incremented by one
    * If not traverse left or right based on the comparison
    * Continue this process until the word is found or a null Node is found
    * If a null Node is found, a new Node is created
    * @param (String) word - The word to be inserted
    * @return (void)
    */
    public synchronized void insertNode(String word) {       
        Node current = root;
        if(root == null) {
            Node node = new Node(word);
            root = node;
            return;
        }
        
        while(current != null) {
            if(word.equals(current.getWord())) {
                current.increment();
                break;
            }
            else if(word.compareTo(current.getWord()) < 0) {
                if(current.getLeft() == null) {
                    Node node = new Node(word);
                    current.setLeft(node);
                    break;
                }
                else {
                    current = current.getLeft();
                }
            }
            else {
                if(current.getRight() == null) {
                    Node node = new Node(word);
                    current.setRight(node);
                    break;
                }
                else {
                    current = current.getRight();
                }
            }
        }
    }
    
    /** Removes a node from the tree
    * Starts at the root
    * If the current Node is the same as the passed in word, decrement the count by one if it is not 0
    * Otherwise, traverse left or right based on the comparison
    * If a null node is found, the word is not in the tree, so quit
    * @param (String) word - The word to be removed from the Node
    * @return (void)
    */
    public synchronized void removeWord(String word) {
        Node current = root;
        boolean found = false;
        while(current != null) {
            if(word.equals(current.getWord())) {
                if(current.getCount() == 0) {
                    Driver.logger.writeMessage(word + " does not exist in the tree", MyLogger.DebugLevel.TREE_EDIT);
                }
                else {
                    Driver.logger.writeMessage(word + " has been removed.", MyLogger.DebugLevel.TREE_EDIT);
                }
                current.decrement();
                found = true;
                break;
            }
            else if(word.compareTo(current.getWord()) < 0) {
                current = current.getLeft();
            }
            else {
                current = current.getRight();
            }
        }
        
        if(!found) {
            Driver.logger.writeMessage(word + " does not exist in the tree", MyLogger.DebugLevel.TREE_EDIT);
        }
    }
    
    /** In Order traversal of the Tree
    * Start at the root Node
    * Continuously traverse left using recursion
    * Once a leaf is found, begin performing calculations for number of words, number of characters, and number of unique words
    * Then traverse right using recursion
    * Continue doing this until all nodes have been traversed
    * @param (Node) current - The current Node in the traversal
    * @return (void)
    */
    public void traverse(Node current) {
        if(current == null) {
            return;
        }
        traverse(current.getLeft());
        numWords = numWords + current.getCount();
        if(current.getCount() != 0) {
            numUnique = numUnique + 1;
        }
        numChars = numChars + (current.getCount()*current.getWord().length());
        traverse(current.getRight());
    }
    
    /** String representation of the Tree
    * @param (void)
    * @return (String) - A string showing the three desired data points of the tree
    */
    public String toString() {
        return "The total number of words: " + numWords +
            "\nThe total number of characters: " + numChars +
            "\nThe total number of unique words: " + numUnique;
    }
}