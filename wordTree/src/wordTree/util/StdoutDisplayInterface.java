package wordTree.driver;

public interface StdoutDisplayInterface {
    public void writeToScreen(String output);
}