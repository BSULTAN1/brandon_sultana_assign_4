package wordTree.driver;

import java.util.ArrayList;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileProcessor {
    
    /** Constructor for FileProcessor
    * @param (void)
    * @return (void)
    */
    public FileProcessor() {
        Driver.logger.writeMessage("Instance of FileProcessor created.", MyLogger.DebugLevel.CONSTRUCTOR);
    }
    
    /** Reads a line from the file
    * Uses the BufferedReader to read a line from the file
    * Catch an exception if one arises
    * @param (BufferedReader) read - The BufferedReader for the file
    * @return (String) line - The line from the file
    */
    public String readLine(BufferedReader read) {
        String line = null;
        try {
            line = read.readLine();
        }
        catch(IOException e) {
            System.err.println("Error: IO Exception");
            System.exit(0);
        }
        
        return line;
    }
    
    
}