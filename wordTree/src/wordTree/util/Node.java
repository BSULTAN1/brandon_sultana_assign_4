package wordTree.driver;

public class Node {
    private int count;
    private String word;
    private Node leftChild;
    private Node rightChild;
    
    /** Constructor for Node
    * @param (String) w - The word to be stored in the node
    * @return (void)
    */
    public Node(String w) {
        Driver.logger.writeMessage("Instance of Node created.", MyLogger.DebugLevel.CONSTRUCTOR);
        word = w;
        count = 1;
        leftChild = null;
        rightChild = null;
    }
    
    /** Accessor for word
    * @param (void)
    * @return (void) word - The word in the Node
    */
    public String getWord() {
        return word;
    }
    
    /** Increments the count of the word by 1
    * @param (void)
    * @return (void)
    */
    public void increment() {
        count = count + 1;
    }
    
    /** Decrements the count of the word by 1
    * @param (void)
    * @return (void)
    */
    public void decrement() {
        if(count != 0) {
            count = count - 1;
        }
    }
    
    /** Accessor for count
    * @param (void)
    * @return (void) count - The number of occurences of a word
    */
    public int getCount() {
        return count;
    }
    
    /** Accessor for leftChild
    * @param (void)
    * @return (void) leftChild - The left child of the node
    */
    public Node getLeft() {
        return leftChild;
    }
    
    /** Mutator for leftChild
    * @param (Node) newLeft - the new left child of the node
    * @return (void)
    */
    public void setLeft(Node newLeft) {
        leftChild = newLeft;
    }
    
    /** Accessor for rightChild
    * @param (void)
    * @return (void) rightChild - The right child of the node
    */
    public Node getRight() {
        return rightChild;
    }
   
    /** Mutator for rightChild
    * @param (Node) newRight - the new right child of the node
    * @return (void)
    */
    public void setRight(Node newRight) {
        rightChild = newRight;
    }
    
    /** String representation of the node
    * @param (void)
    * @return (String) - A string containing the word and its number of occurences
    */
    public String toString() {
        return "Word: " + word + "\nCount: " + count;
    }
    
    
}